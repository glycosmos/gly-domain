# switch domain js
packeage.json
```
"dependencies": {
    "gly-domain": "git+https://gitlab.com/glycosmos/gly-domain.git",
},
```

use getdomain function. Ex,

```
import getdomain from 'gly-domain/gly-domain.js';

const host =  getdomain(location.href);
const url1 = 'https://'+ host +'/sparqlist/api/gtc_summary?accNum=' + this.accession;
```

URL文字列に"test"が含まれていればtest環境に、含まれていなければ本番環境のsparqlistのドメインを参照します。  
URLに#testをつけることでテストできます。
