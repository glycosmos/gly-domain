  export default function getDomain(hostname) {
    hostname = location.href;
    console.log("URL: " + hostname)
      let domain = "sparqlist.glytoucan.org";
      if (hostname.match(/alpha/) ){
          domain = "sparqlist.alpha.glytoucan.org";
      }
      if (hostname.match(/beta/) ){
          domain = "sparqlist.beta.glytoucan.org";
      }
    return domain;
  }
  export function getApiDomain(hostname) {
    hostname = location.href;
    console.log("URL: " + hostname)
      let domain = "api.glycosmos.org";
      if (hostname.match(/alpha/) ){
          domain = "api.alpha.glycosmos.org";
      }
      if (hostname.match(/beta/) ){
          domain = "api.beta.glycosmos.org";
      }
    return domain;
  }
